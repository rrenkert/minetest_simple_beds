minetest.register_node("simple_beds:bed_bottom", {
	description = "Bed",
	inventory_image = "beds_bed.png",
	wield_image = "beds_bed.png",
	wield_scale = {x=0.8,y=2.5,z=1.3},
	drawtype = "nodebox",
	tiles = {"beds_bed_top_bottom.png^[transformR90", "default_wood.png",  "beds_bed_side_bottom_r.png",  "beds_bed_side_bottom_r.png^[transformfx", "beds_bed_leer.png", "beds_bed_side_bottom.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	stack_max = 1,
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
	sounds = default.node_sound_wood_defaults(),
	node_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 0.06, 0.5},
	},
	selection_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 0.06, 1.5},
				
	},
	
	after_place_node = function(pos, placer, itemstack)
		local node = minetest.env:get_node(pos)
		local param2 = node.param2
		local npos = {x=pos.x, y=pos.y, z=pos.z}
		if param2 == 0 then
			npos.z = npos.z+1
		elseif param2 == 1 then
			npos.x = npos.x+1
		elseif param2 == 2 then
			npos.z = npos.z-1
		elseif param2 == 3 then
			npos.x = npos.x-1
        end
        local name = minetest.env:get_node({x=npos.x, y=npos.y-1, z=npos.z}).name
		if minetest.registered_nodes[minetest.env:get_node(npos).name].buildable_to == true and (name ~= "air" or not name:match("syntheticsun:*"))then
			minetest.env:set_node(npos, {name="simple_beds:bed_top", param2 = param2})
		else
			minetest.env:dig_node(pos)
			return true
		end
	end,	
	
	on_destruct = function(pos)
		pos = minetest.env:find_node_near(pos, 1, "simple_beds:bed_top")
		if pos ~= nil then minetest.env:remove_node(pos) end
	end	
})

minetest.register_node("simple_beds:bed_top", {
	drawtype = "nodebox",
	tiles = {"beds_bed_top_top.png^[transformR90", "beds_bed_leer.png",  "beds_bed_side_top_r.png",  "beds_bed_side_top_r.png^[transformfx",  "beds_bed_side_top.png", "beds_bed_leer.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3},
	sounds = default.node_sound_wood_defaults(),
	node_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 0.06, 0.5},
	},
	selection_box = {
		type = "fixed",
		fixed = {0, 0, 0, 0, 0, 0},
	},
})

minetest.register_alias("simple_beds:bed", "simple_beds:bed_bottom")

minetest.register_craft({
	output = "simple_beds:bed",
	recipe = {
		{"group:wool", "group:wool", "group:wool", },
		{"default:stick", "", "default:stick", }
	}
})
