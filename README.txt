Minetest mod "Simple_Beds"
=======================
version: 1.0


License of source code: WTFPL
-----------------------------
stripped by: Sascha Wilde (2014)
author: BlockMen (2013)
original author: PilzAdam

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.


--USING the mod--
------------------

This mods implements Beds as simple furniture without any semantics.
